package com.example.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = { "com.example.practice.domain" })
public class MyPracticeApplication  {

	public static void main(String[] args) {
		SpringApplication.run(MyPracticeApplication.class, args);
	}

	
}
