package com.example.practice.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//@Entity
@Table(schema = "TEST")
public class EmployeeCourses {
	@Id
	@GeneratedValue
	private long id;

	public EmployeeCourses(String couserName) {
		super();
		this.couserName = couserName;
	}

	public EmployeeCourses() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String couserName;
	@ManyToMany(mappedBy = "courseList",cascade = CascadeType.ALL)
	private List<Employee> emp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCouserName() {
		return couserName;
	}

	public void setCouserName(String couserName) {
		this.couserName = couserName;
	}

	public List<Employee> getEmp() {
		return emp;
	}

	public void setEmp(List<Employee> emp) {
		this.emp = emp;
	}

	
	
}
