package com.example.practice.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "TEST")
public class EmployeeDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long empDetId;

	private long empAge;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "emp_id", referencedColumnName = "id")
	private Employee employee;

	public Long getEmpDetId() {
		return empDetId;
	}

	public void setEmpDetId(Long empDetId) {
		this.empDetId = empDetId;
	}

	public long getEmpAge() {
		return empAge;
	}

	public void setEmpAge(long empAge) {
		this.empAge = empAge;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
