package com.example.practice.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema = "TEST")
public class Teacher {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long teacher_id;

	private String teacher_name;

	
	/*
	 * @OneToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinColumn(name = "teacher_id") private List<Course> courseList;
	 */

	public long getTeacher_id() {
		return teacher_id;
	}

	public void setTeacher_id(long teacher_id) {
		this.teacher_id = teacher_id;
	}

	public String getTeacher_name() {
		return teacher_name;
	}

	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}

	/*
	 * public List<Course> getCourseList() { return courseList; }
	 * 
	 * public void setCourseList(List<Course> courseList) { this.courseList =
	 * courseList; }
	 */
}
