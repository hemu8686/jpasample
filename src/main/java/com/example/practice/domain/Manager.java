package com.example.practice.domain;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class Manager {
	private String empManager;

	private String empManagerId;

	public String getEmpManager() {
		return empManager;
	}

	public void setEmpManager(String empManager) {
		this.empManager = empManager;
	}

	public String getEmpManagerId() {
		return empManagerId;
	}

	public void setEmpManagerId(String empManagerId) {
		this.empManagerId = empManagerId;
	}
}
