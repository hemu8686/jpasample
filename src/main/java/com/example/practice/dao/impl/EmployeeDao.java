package com.example.practice.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDao {

	private EntityManagerFactory emf;

	public EmployeeDao(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManagerFactory() {
		return emf.createEntityManager();
	}


}
