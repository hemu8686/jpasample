package com.example.practice.dao.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.practice.domain.Teacher;
@Repository
public interface TeacherRepo extends JpaRepository<Teacher, Long> {

}
