package com.example.practice.dao.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.practice.domain.Employee;
@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	
	public Employee getEmpByIdAndEmpName(long id,String name);

}
