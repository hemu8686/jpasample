package com.example.practice.dao.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.practice.domain.EmployeeDetails;
@Repository
public interface EmployeeDetailsRepo extends JpaRepository<EmployeeDetails, Long> {
	
	public EmployeeDetails getEmpDetByempDetId(long id);

}
