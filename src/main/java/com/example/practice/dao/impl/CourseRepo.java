package com.example.practice.dao.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.practice.domain.Course;
@Repository
public interface CourseRepo extends JpaRepository<Course, Long> {
	
	public Course getCourseBycourseId(long id);

}
