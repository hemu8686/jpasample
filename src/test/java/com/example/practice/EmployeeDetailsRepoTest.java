package com.example.practice;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.practice.dao.impl.EmployeeDetailsRepo;
import com.example.practice.domain.Employee;
import com.example.practice.domain.EmployeeDetails;

@SpringBootTest
public class EmployeeDetailsRepoTest {
	
	
	@Autowired
	private EmployeeDetailsRepo employeeRepo;
	
	@Test
	public void saveEmployee() {
		Employee e = new Employee();
		e.setEmpName("Rahul");
		EmployeeDetails ed=new EmployeeDetails();
		ed.setEmpAge(21);
		ed.setEmployee(e);
		EmployeeDetails save = employeeRepo.save(ed);
		assertThat(save.getEmpAge()).isEqualTo(ed.getEmpAge());
	}
	
	
	/*
	 * @Test public void delete() { employeeRepo.deleteById(Long.valueOf(34)); }
	 */
	
	@Test
	public void get() {
		EmployeeDetails byId = employeeRepo.getEmpDetByempDetId(Long.valueOf(1));
		System.err.println(byId);
	//	assertThat(byId.getEmpAge()).isEqualTo(ed.getEmpAge());
	}
}
