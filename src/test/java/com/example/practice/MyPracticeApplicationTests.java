package com.example.practice;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.practice.dao.impl.EmployeeRepo;
import com.example.practice.dao.impl.ProjectRepo;
import com.example.practice.domain.Employee;
import com.example.practice.domain.Manager;
import com.example.practice.domain.Project;

@SpringBootTest
class MyPracticeApplicationTests {

	@Autowired
	private EmployeeRepo employeeRepo;

	@Autowired
	private ProjectRepo projectRepo;

	@Test
	public void saveEmployee() {
		Employee e = new Employee();
		Manager manager = new Manager();
		e.setEmpName("Hemantj");
		manager.setEmpManager("Kumar");
		manager.setEmpManagerId("123");
		e.setManager(manager);
		Employee saveAndFlush = employeeRepo.saveAndFlush(e);
		assertThat(saveAndFlush.getEmpName()).isEqualTo(e.getEmpName());
	}

	@Test
	public void saveProject() {
		Project p = new Project();
		p.setProjectName("SCB");
		Project saveAndFlush = projectRepo.saveAndFlush(p);
		assertThat(saveAndFlush.getProjectName()).isEqualTo(p.getProjectName());

	}

}
