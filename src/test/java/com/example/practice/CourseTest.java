package com.example.practice;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.practice.dao.impl.CourseRepo;
import com.example.practice.domain.Course;
import com.example.practice.domain.Employee;
import com.example.practice.domain.Teacher;

@SpringBootTest
public class CourseTest {
	
	@Autowired
	private CourseRepo courseRepo;
	
	/*
	 * @Test public void getData() { // Course byId =
	 * courseRepo.getById(Long.valueOf(26)); Course byId =
	 * courseRepo.getCourseBycourseId(Long.valueOf(26));
	 * assertThat(byId.getCourse_name()).isEqualTo("AWS"); }
	 */
	
	@Test
	public void save() {
		Teacher t=new Teacher();
		t.setTeacher_name("Donald");
		Course c=new Course();
		c.setTeacher(t);
		c.setCourse_name("JAVA");
		Employee e = new Employee();
		e.setEmpName("RahulRama");
		c.setEmpList(Arrays.asList(e));
		Course save = courseRepo.save(c);
		assertThat(save.getCourse_name()).isEqualTo(c.getCourse_name());
	}
	

}
