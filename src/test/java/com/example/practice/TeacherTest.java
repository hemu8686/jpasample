package com.example.practice;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.practice.dao.impl.TeacherRepo;
import com.example.practice.domain.Course;
import com.example.practice.domain.Teacher;

@SpringBootTest
public class TeacherTest {
	
	@Autowired
	private TeacherRepo teacherRepo;
	
	@Test
	public void save() {
		List<Course> cList=new ArrayList<Course>(Arrays.asList(new Course("AWS")));
		Teacher t=new Teacher();
	//	t.setCourseList(cList);
		t.setTeacher_name("Donald");
		Teacher save = teacherRepo.save(t);
		assertThat(save.getTeacher_name()).isEqualTo(t.getTeacher_name());
	}
	
	
	

}
